﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodMobController : MonoBehaviour
{
    private bool isWill;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Hero")
        {
            isWill = true;
        }
    }
    private void FixedUpdate()
    {
        if(isWill)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,200f));
            isWill = false;
        }
        
    }
}
