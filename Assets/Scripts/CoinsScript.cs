﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsScript : MonoBehaviour
{
    private bool isTake;

    private void FixedUpdate()
    {
        if(isTake)
        {
            gameObject.transform.position -= new Vector3(0,4*Time.deltaTime,0);
        }
        if(gameObject.transform.position.y<=-7f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Hero")
        {
            isTake = true;
        }
    }
    
}
