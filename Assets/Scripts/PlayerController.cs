﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _playerPS;
    private Animator _heroAminator;
    [SerializeField]
    private int _speed;

    private bool isStairs;

    private bool isGround;

    private bool isMove;

    private void Start()
    {
        _heroAminator = gameObject.GetComponent<Animator>();
        EventManager._PLAYERMOVE += GetMovePlayer;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NextLevel2")
        {
            SceneManager.LoadScene("Scene2");
        }
        if (collision.gameObject.tag == "Win")
        {
            EventManager._VICTORY(new EventData(true));
        }
        if (collision.gameObject.tag == "Stairs")
        {
            isStairs = true;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            isGround = true;
        }
        if (collision.gameObject.tag == "Jump")
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(50f, 400f));
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            gameObject.transform.rotation = new Quaternion(0f, 175f, 0f, 0f);

            if (isMove || transform.position.x > 0)
            {
                gameObject.transform.position -= new Vector3(_speed * Time.deltaTime, 0, 0);
            }
            _heroAminator.SetBool("isMove", true);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);

            if (isMove || transform.position.x < 0)
            {
                gameObject.transform.position += new Vector3(_speed * Time.deltaTime, 0, 0);
            }

            _heroAminator.SetBool("isMove", true);
        }
        if (!Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
        {
            _heroAminator.SetBool("isMove", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            _heroAminator.SetBool("isAttack", true);
        }
        else
        {
            _heroAminator.SetBool("isAttack", false);
        }

        if (Input.GetKey(KeyCode.Space) && isStairs)
        {

            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 2f), ForceMode2D.Impulse);
            isStairs = false;

        }
        if (Input.GetKey(KeyCode.Space) && isGround)
        {

            _heroAminator.SetBool("isJump", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 250f));

            isGround = false;

        }

    }

    private void GetMovePlayer(IData data)
    {
        isMove = (bool)((EventData)data).data;
    }
}
