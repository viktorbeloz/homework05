﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    interface IEvent
    {

    }
    public class EventManager
    {
        public delegate void VICTORY(IData data);
        public delegate void PLAYERMOVE(IData data);

        static public VICTORY _VICTORY;
        static public PLAYERMOVE _PLAYERMOVE;

    }
}
