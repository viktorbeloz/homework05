﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatformManager : MonoBehaviour
{
    [SerializeField]
    private int _speed;
    [SerializeField]
    private Vector3 _finishedPos;

    private Vector3 _startPos;
    void Start()
    {
        _startPos = gameObject.transform.position;
    }

    void Update()
    {
        if (transform.position.y < 1.9f)
            transform.position += new Vector3(0f, _speed * Time.deltaTime, 0f);
    }
}
