﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    
    [SerializeField]
    private GameObject _victoryBG;
    [SerializeField]
    private Button _restartButton;
    private void Awake()
    {
        EventManager._VICTORY += VictoryFunc;
        _restartButton.onClick.AddListener(RestartClick);
    }

    private void VictoryFunc(IData data)
    {
        bool isVictory = (bool)((EventData)data).data;

        if(isVictory)
        {
            _victoryBG.SetActive(true);
        }
    }
    private void RestartClick()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
