﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static iTween;

public class AnimManager : MonoBehaviour
{
    [SerializeField]
    private Vector3 _toPosition;
    [SerializeField]
    private LoopType _loopType;
    [SerializeField]
    private EaseType _easeType;
    [SerializeField]
    private float _time;
    [SerializeField]
    private bool _playOnAwake;

    private Hashtable _ht;
    private Vector3 _currentPos;

    void Start()
    {
        _ht = iTween.Hash(
        "x", _toPosition.x,
       "y", _toPosition.y,
        "z", _toPosition.z,
        "isLocal", true,
        "time", _time,
        "LoopType", _loopType,
        "EaseType", _easeType
    //"OnComplete", "OnComFinish"

    );
        if (_playOnAwake)
        {
            iTween.MoveTo(gameObject, _ht);
        }
    }
    public Hashtable GetHT()
    {
        return _ht;
    }

}
