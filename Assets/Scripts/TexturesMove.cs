﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexturesMove : MonoBehaviour
{
    [SerializeField]
    private Vector2 _startPos;
    [SerializeField]
    private Vector2 _endPos;

    [SerializeField]
    private GameObject _player;

    private void FixedUpdate()
    {
        if(_player.transform.position.x>=0 && Input.GetKey(KeyCode.RightArrow))
        {
            if(_endPos.x < transform.position.x)
            {
                gameObject.transform.position -= new Vector3(3 * Time.deltaTime, 0, 0);
                EventManager._PLAYERMOVE(new EventData(false));
            } 
            else
            {
                EventManager._PLAYERMOVE(new EventData(true));
            }
        }
        if (_player.transform.position.x <= 0 && Input.GetKey(KeyCode.LeftArrow))
        {
            if(_startPos.x > transform.position.x)
            {
                gameObject.transform.position += new Vector3(3 * Time.deltaTime, 0, 0);
                EventManager._PLAYERMOVE(new EventData(false));
            }
            else
            {
                EventManager._PLAYERMOVE(new EventData(true));
            }
            
        }
    }

}
